DEGREE_INCREMENTS = 5;
SCALE_INCREMENTS = 20;
POSITION_INCREMENT = 15;
DEFAULT_ROTATE = 0;
DEFAULT_ROTATE_Y = 0;
DEFAULT_SCALE = 1.0;
DEFAULT_SCALE_X = 1.0;
DEFAULT_SCALE_Y = 1.0;

current_rotate = DEFAULT_ROTATE;
current_rotate_y = DEFAULT_ROTATE_Y;
current_scale = DEFAULT_SCALE;
current_scale_x = DEFAULT_SCALE_X;
current_scale_y = DEFAULT_SCALE_Y;

event_listeners_added = false;


var video_elements = document.getElementsByTagName("video");

//set initial position so we can parse it for relative movement
set_position("reset");

function transform() {
	for (var i=0; i < video_elements.length; i++) {
		var transform = "";
		transform += " scale(" + current_scale + ") ";
		transform += " scaleX(" + current_scale_x + ") ";
		transform += " scaleY(" + current_scale_y + ") ";
		transform += " rotate(" + current_rotate + "deg) ";
		transform += " rotateY(" + current_rotate_y + "deg) ";
		video_elements[i].style.transform=transform;
	}
}

function set_position(command, param=0) {
	for (var i=0; i < video_elements.length; i++) {
		switch (command) {
			case 'left':
				video_elements[i].style.left = (parseInt(video_elements[i].style.left,10) + param) + 'px';
				break;
			case 'top':
				video_elements[i].style.top = (parseInt(video_elements[i].style.top,10) + param) + 'px';
				break;
			case 'reset':
				video_elements[i].style.top = 0 + 'px';
				video_elements[i].style.left = 0 + 'px';
				break;
		}
	}
}

//rotate counter clockwise
function rotate_counter_clockwise(event) {
	if (event.code == "Numpad1" && !event.ctrlKey && event.altKey && !event.shiftKey && !event.metaKey) {
		current_rotate -= DEGREE_INCREMENTS
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//rotate clockwise
function rotate_clockwise(event) {
	if (event.code == "Numpad3" && !event.ctrlKey && event.altKey && !event.shiftKey && !event.metaKey) {
		current_rotate += DEGREE_INCREMENTS
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//flip left to right
function flip_horizontal(event) {
	if ((event.code == "Numpad4" || event.code == "Numpad6") && !event.ctrlKey && event.altKey && !event.shiftKey && !event.metaKey) {
		current_rotate_y = (current_rotate_y == 0) ? 180 : 0;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//flip up to down
function flip_vertical(event) {
	if ((event.code == "Numpad8" || event.code == "Numpad2") && !event.ctrlKey && event.altKey && !event.shiftKey && !event.metaKey) {
		current_rotate_y = (current_rotate_y == 0) ? 180 : 0;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//position up
function position_up(event) {
	if (event.code == "Numpad8" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("top", -POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position up+right
function position_up_right(event) {
	if (event.code == "Numpad9" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("left", POSITION_INCREMENT);
		set_position("top", -POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position right
function position_right(event) {
	if (event.code == "Numpad6" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("left", POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position down+right
function position_down_right(event) {
	if (event.code == "Numpad3" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("left", POSITION_INCREMENT);
		set_position("top", POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position down
function position_down(event) {
	if (event.code == "Numpad2" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("top", POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position down+left
function position_down_left(event) {
	if (event.code == "Numpad1" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("left", -POSITION_INCREMENT);
		set_position("top", POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position left
function position_left(event) {
	if (event.code == "Numpad4" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("left", -POSITION_INCREMENT);
		event.preventDefault();
		event.stopPropagation();
	}
}

//position up+left
function position_up_left(event) {
	if (event.code == "Numpad7" && event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		set_position("left", -POSITION_INCREMENT);
		set_position("top", -POSITION_INCREMENT);
		event.preventDefault();
	}
}

//increase stretch on x-axis
function increase_stretch_x(event) {
	if (event.code == "Numpad6" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_scale_x += current_scale_x / SCALE_INCREMENTS;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//decrease stretch on x-axis
function decrease_stretch_x(event) {
	if (event.code == "Numpad4" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_scale_x -= current_scale_x / SCALE_INCREMENTS;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//increase stretch on y-axis
function increase_stretch_y(event) {
	if (event.code == "Numpad8" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_scale_y += current_scale_y / SCALE_INCREMENTS;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//decrease stretch on y-axis
function decrease_stretch_y(event) {
	if (event.code == "Numpad2" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_scale_y -= current_scale_y / SCALE_INCREMENTS;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//zoom in
function zoom_in(event) {
	if (event.code == "Numpad9" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_scale += current_scale / SCALE_INCREMENTS;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//zoom out
function zoom_out(event) {
	if (event.code == "Numpad1" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_scale -= current_scale / SCALE_INCREMENTS;
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//reset
function reset(event) {
	if (event.code == "Numpad5" && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		current_rotate = DEFAULT_ROTATE;
		current_rotate_y = DEFAULT_ROTATE_Y;
		current_scale = DEFAULT_SCALE;
		current_scale_x = DEFAULT_SCALE_X;
		current_scale_y = DEFAULT_SCALE_Y;
		set_position("reset")
		transform();
		event.preventDefault();
		event.stopPropagation();
	}
}

//override youtube hotkeys (on their website they claim not to use the numpad, but they do...)
function remove_youtube_hotkeys(event) {
	if ((event.code == "Numpad7" || event.code == "Numpad3") && !event.ctrlKey && !event.altKey && !event.shiftKey && !event.metaKey) {
		event.preventDefault();
		event.stopPropagation();
	}
}

var events = [rotate_counter_clockwise, rotate_clockwise, flip_horizontal, flip_vertical, position_up,
	position_up_right, position_right, position_down_right, position_down, position_down_left, position_left,
	position_up_left, increase_stretch_x, decrease_stretch_x, increase_stretch_y, decrease_stretch_y, zoom_in,
	zoom_out, reset, remove_youtube_hotkeys]
	
function add_event_listeners() {
	event_listeners_added = true;
	for(var i = 0; i < events.length; i++) {
		window.addEventListener("keydown",  events[i], true);
	}
}

function remove_event_listeners() {
	for(var i = 0; i < events.length; i++) {
		window.removeEventListener("keydown",  events[i], true);
	}
	event_listeners_added = false;
}

for (var i=0; i < video_elements.length; i++) {
	video_elements[i].addEventListener("mouseover", function(event) {
		if (event_listeners_added == false) {
			add_event_listeners();
		}
	}, true);
}

for (var i=0; i < video_elements.length; i++) {
	video_elements[i].addEventListener("mouseout", function(event) {
		if (event_listeners_added == true) {
			remove_event_listeners();
		}		
	}, true);
}

//TODO: (if someone requests it, not sure if I personally need it)
//	-make hotkeys configurable